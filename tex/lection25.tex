\setauthor{М. Минин}

\par Рассмотрим комплексный вектор:
\[
\conj F = F_x - i F_y = - i \oint\limits_l \left( \strut p \, dx - p i \, dy \right)
	= - i \oint\limits_l p \, d\conj z
\]

\par Из уравнения Бернулли:

\[
    \frac{|v|^2}{2} + \frac{p}{\rho} = C \quad \Rightarrow \quad p = \rho C - \rho \frac{|v|^2|}{2}
\]

\par Подставляем в интеграл и получаем:

\[
\conj F = - i \oint\limits_l p \ d\conj z = - i\left[ \oint\limits_l \rho C \ d \conj z - \oint\limits_l \rho \frac{|v|^2}{2} \ d\conj z\right] = i \frac{\rho}{2} \oint\limits_l |v|^2 \ d\conj z
\]

\par Первый интеграл равен нулю, т.к. под интегралом стоит полный дифференциал, а интегрируем по замкнутому контуру.

\begin{wrapfigure}[8]{l}{6cm}
\vspace{0pt}
\begin{tikzpicture}[scale = 2.6]
    \draw[thick] (0, 0) --++(1, 0);
    \draw[thick, dashed] (1, 0) --++(1, 0);
    \draw[thick] (1,0) arc (60 : 80 : 3);
    \draw[thick, dashed] (1.5, 0) to ++ (166 : 1.5);
    \draw[thick, ->] (.2, .24) --(.6, .14) node[midway, below] {$v$};
    \draw (.25, .33) node[right = 2pt, above= 1pt] {$dl$};
    \draw [->, blue] (1.7, 0) arc (0: 166 : .2) node[midway, above] {$\beta$};
\end{tikzpicture}
\end{wrapfigure}

\par Рассмотрим кусок контура:
\[
	dz = dx + idy = dl \ e^{i\beta} \quad dl = dz \ e^{-i\beta}
\]

\par Тогда
\[
	d\conj z = dl \ e^{-i\beta} = e^{-2i\beta} dz
\]

\par Аналогично распишем для скорости:
\[
\left.
    \begin{aligned}
    v_x + i v_y = |v| e^{-i(\pi - \beta)} = - |v| e^{i\beta}&\\
    \conj v = - |v| e^{-i\beta}&
    \end{aligned}
\right| \quad \Rightarrow \quad v = -\conj v e^{i\beta} \quad \Rightarrow \quad v^2 = \conj v^2 e^{2 i \beta}
\]

\par Мы только что научились переносить сопряжение с квадрата скорости на дифференциал координаты. Проделаем это:
\[
	 |v|^2\, d\conj z = \conj v^2 e^{2i\beta}\cdot  d\conj z = \conj v ^2 \, d z
\]
\par Значит имеет место следующие равенство:

\[
	\conj F = i \frac{\rho}{2} \oint\limits_l |v|^2 \ d\conj z = i\frac{\rho}{2} \oint\limits_l \conj v^2 dz
\]

\par Подставив $\conj v = \dif{\omega}{z}$ получим первую формулу Чаплыгина-Блазиуса:

\begin{equation}
\myboxed{
	\conj F = i \frac{\rho}{2} \oint \left( \dif{\omega}{z}\right)^2 dz
}
\end{equation}


\begin{wrapfigure}[15]{r}{5cm}
\centering
\begin{tikzpicture}[scale = 2]
    \draw [->] (-.2, 0) --++(2.2, 0) node[below] {$x$};
    \draw [->] (0, -.2) --++(0, 2.2) node[left] {$y$};
    \draw [thick, dotted] (1, 0) node[below] {$x$} -- (1, 1) -- (0, 1) node[left] {$y$};
    \draw [line width = 3pt] (.9, 1.1) -- (1.1, .9) node[midway, below left] {$dl$};
    \draw [thick, ->] (1, 1) --++(.5, 0) node[above] {$dF_x$};
    \draw [thick, ->] (1, 1) --++(0, .5) node[left] {$dF_y$};
\end{tikzpicture}
\end{wrapfigure}

\par Получим похожий интеграл для главного момента сил, действующих на контур:


\[	
	dL = dF_y \cdot x - dF_x \cdot y
\]

\par Воспользуемся выражениями (20.54):

\[
	dL = p x \, dx + p y \, dy = p \left(x \, dx + y \, dy\right)
\]

\par Тогда момент будет равен:

\[
	L = \oint\limits_l dL = \oint\limits_l p \left(x \, dx + y \, dy \strut\right) = \oint\limits_l p d \left( \frac{x^2 + y^2}{2}\right)
\]

\par Теперь рассмотрим следующее выражение:

\[
	z \ d\conj z = (x + i y)(dx - i dy) = x \ dx + y \ dy + i(y\ dx - x\ dy)
\]

\par Таким образом получили, что
\[
	x \ dx + y\ dy = \Real(z \ d\conj z)
\]

\par Как всегда воспользуемся уравнением Бернулли:

\[
L = \rho C \oint\limits_l d \left( \frac{x^2 + y^2}{2}\right) - \frac{\rho}{2} \oint\limits_l |v|^2 \Real (z \, d \conj z)
=
\Real\left( - \frac{\rho}{2} \oint\limits_l |v|^2 z \ d\conj z \right)
\]

\par Первое слагаемое обращается в ноль, так как это опять полный дифференциал. В силу того, что $|v|^2 d\conj z = \conj v^2 dz$

\[
L = \Real\left( - \frac{\rho}{2} \oint\limits_l \conj v^2 z \ dz \right)
\]

Таким образом, получили вторую формулу Чаплыгина-Блазиуса:

\begin{equation}
\myboxed{	
	L = \Real \left( - \frac{\rho}{2} \oint\limits_l \left( \dif{\omega}{z}\right)^2 z \, dz\right)
}
\end{equation}

\subsubsection{Теорема Жуковского.}

\begin{wrapfigure}{l}{4cm}
\begin{tikzpicture}
	\draw [->, thick] (-2, 0) -- (2, 0);
	\draw [->, thick] (0, -1) -- (0, 2);
	\draw [very thick, postaction={
			decorate, decoration = { markings,
					mark = at position .5 with { \arrowreversed{stealth} },
					mark = at position 0 with { \arrowreversed{stealth} }
		  } }]
					(-1, 0) .. controls (-1, 1) and (0.8,-.2) .. (1.5, -.2);
	\draw [very thick, marrow]
					(-1, 0) .. controls (-1, -.5) and (0.1,-.2) .. (1.5, -.2) node [pos=.2, below] {$l$};
    \draw [very thick, postaction={
    	decorate, decoration={markings,
	     mark = between positions 0 and 1 step 0.2 with { \arrow{stealth} }
	     } } ]
				     (1.6, 0) arc (0 : -360 : 1.6);
    \draw (45 : 1.3) node {$c$};
    \draw [very thick] ({1.6* cos(135)}, {1.6*sin(135)}) -- (-.7, .4);
\end{tikzpicture}
\end{wrapfigure}
\par Рассмотрим произвольный профиль и воспользуемся интегральной теоремой Коши: если $F(z)$~--- регулярна на множестве $\conj D = D \cup \partial D$ и контур $\gamma \in \conj D$, то $\oint\limits_\gamma F(z) dz = 0$

\par Возьмем контур $l$ и окружность $c$, делаем разрез между ними и обходим исходный контур против часовой, а внешний~--- по часовой. Тогда условно можно разделить контур $\gamma = l + c + \text{разрез}$. Пусть $F(z) = \dif{\omega}{z}$, тогда:

\[
	\oint\limits_\gamma \dif\omega z dz = \oint\limits_l \dif \omega z dz + \oint\limits_c \dif\omega z dz + \cancel{\oint\limits_\text{р} \dif\omega z dz} = 0
\]

\par Интеграл по разрезу равен нулю, т.к. по разрезу мы прошли два раза в разные стороны.

\par Аналогично проделаем для $F(z) = \left( \dif{\omega}{z}\right)^2$:

\[
\oint\limits_l \left( \dif{\omega}{z}\right)^2 dz + \oint\limits_c \left( \dif{\omega}{z}\right)^2 dz =0
\]

\par Еще раз запишем эти равенства:

\begin{equation}
\begin{aligned}
	\oint\limits_l \dif \omega z dz &= - \oint\limits_c \dif\omega z dz\\
	\oint\limits_l \left( \dif{\omega}{z}\right)^2 dz &= -\oint\limits_c \left( \dif{\omega}{z}\right)^2 dz
\end{aligned}
\end{equation}

\par Разложим $\dif{\omega}{z} = \conj v$ в ряд Лорана:

\[
	\dif\omega z = \conj v = A_0 + A_1\frac{1}{z} + A_2 \frac{1}{z^2} + \dots
\]

\par Напоминаем что вычет в бесконечно удаленной точке по определению равен:

\[
	\Res\left.\left(\dif\omega z\right)\right|_{\infty} = \frac{1}{2\pi i} \oint\limits_c \left(\dif\omega z\right) \ dz
\]

\par В нашем случае:

\[
\Res\left.\left(\dif\omega z \right) \right|_{\infty} = -A_1
\]

\par Тогда

\[
\oint\limits_c \left(\dif\omega z \right) \ dz = 2\pi i \Res\left.\left( \dif\omega z\right) \right|_{\infty} = -2\pi i A_1 \qquad (*)
\]

\par Получили один из интегралов из выражения (20.56)

\par Получим ряд Лорана для $\left(\dif\omega z \right)^2$. Для этого перемножим два ряда Лорана для $\dif\omega z$:

\[
\left(\dif\omega z\right)^2 = A_0^2 + 2 A_0A_1\frac{1}{z} + \dots
\]

\par Тогда:

\[
\oint\limits_c \left(\dif \omega z \right)^2 \ dz = 2\pi i \Res\left.\left(\dif\omega z\right)\right|_\infty = -4 \pi i A_0 A_1
\]

\par Получили еще один из интегралов выражения (20.56).

\par Посмотрим, что происходит при больших $z$. При $|z| \rightarrow \infty$ $\left.\conj v\strut \right|_\infty = \conj v^{\infty} = v_{\infty_x} - iv_{\infty_y}$. Тогда получаем, что $A_0 = \conj v^{\infty}$.

\par Распишем интеграл по обтекаемому контуру $l$ другим способом. Воспользуемся тем, что $\omega = \varphi + i\psi$ :

\[
\oint\limits_l \left( \dif{\omega}{z}\right)\ dz = \oint\limits_l d\omega = \oint\limits_l d\varphi + i \oint\limits_l d\psi = \oint\limits_l \partd{\varphi}{l} \ dl = \oint\limits_l v_l \ dl = \Gamma
\]

\par Один из интегралов ушел, т.к. $\psi = const$ на обтекаемом контуре.

\par Тогда из (*) получаем выражение для коэффициента $A_1$:

\[
A_1 = \frac{\Gamma}{2\pi i}
\]

\par Теперь знаем выражение для коэффициентов. Подставим эти выражения в контурный интеграл:

\[
\oint\limits_l \left( \dif\omega z\right)^2 dz = 4\pi i\,  A_0 A_1 = 4\pi i \conj v^{\infty} \frac{\Gamma}{2\pi i} = 2 \conj v^{\infty} \Gamma
\]

\par Подставим полученное выражение в первую формулу Чаплыгина-Блазиуса (20.54)

\begin{equation}
\conj F = \frac{i \rho}{2} \cdot 2\conj v^{\infty} \Gamma = i \rho \conj v^{\infty} \Gamma
\end{equation}

\par Распишем по компонентам:

\[
\conj F = i \rho (v_{\infty_x} - i v_{\infty_y}) \Gamma = \rho v_{\infty_y} \Gamma + i \rho v_{\infty_x} \Gamma = F_x - i F_y
\]

\par Отсюда получаем, что:

\begin{equation}
    F = - i \rho v^{ \infty }\Gamma
\end{equation}

\par Также знаем значение модуля:

\begin{equation}
    |\conj F| = |F| = \rho v_\infty |\Gamma|
\end{equation}

\par Рассмотрим скалярное произведение векторов $\pmb F = (\rho v_{\infty_y} \Gamma, - \rho v_{\infty_x} \Gamma)$ и $\pmb v_\infty = (v_{\infty_x}, v_{\infty_y})$:

\[
\pmb F \cdot \pmb v_{\infty} = \rho v_{\infty_y} \Gamma v_{\infty_x} - \rho v_{\infty_x} \Gamma v_{\infty_y} = 0
\]

\begin{wrapfigure}{l}{4cm}
\begin{tikzpicture}
	\draw [->, thick] (-1.5,0) -- (2,0);
	\draw [->, thick] (0,-1) -- (0,1.5);
	\draw [thick] 	(-1, 0) .. controls (-1, 1) and (0.8,-.2) .. (1.5, -.2)
						node [pos=.2, above] {$l$}
					(-1, 0) .. controls (-1, -.5) and (0.1,-.2) .. (1.5, -.2)
				node [below] {$B$};
    \draw [dashed] (-2, -1) -- (1,1);
    \draw [->, thick] (-2, -1)  --++(1,2/3) node [pos=.2, right=.2] {$\pmb v_{\infty}$};
    \draw [->, thick] (0, 1/3-.1) -- (-2/3, 1) node[above] {$\pmb F$};
\end{tikzpicture}
\end{wrapfigure}
\par Получается, что в рамках нашей теории сила действующая на контур перпендикулярна направлению набегающего потока, то есть мы можем найти подъемную силу. Однако, никакой силы сопротивления не наблюдается. Это так называемый \textbf{парадокс Д'Аламбера}.

\par  Полученный результат можно интерпретировать так: у <<хорошего>> потока сила сопротивления во много раз меньше подъемной силы.

По идее, для момента необходимо снова решать через неопределенные коэффициенты ряда Лорана, но это долго, и мы это уже делали. Поэтому просто выпишем ответ для преобразования $z = f(\zeta)$:

\[
z = f(\zeta) = k\zeta + k_0 + \frac{k_1}{\zeta} + \frac{k_2}{\zeta^2}, \quad k \in\mathbb{R}
\]

Тогда выражение для момента:

\begin{equation}
L = - \Real\left( k_0 \rho \conj v^{\infty} \Gamma + 2\pi i \ k k_1 \rho \left(\conj v^{\infty}\right)^2\right)
\end{equation}



\subsection{Обтекание пластинки под углом атаки.}

\begin{center}
\begin{tikzpicture}
	\draw [->] (-2, 0) -- (2, 0) node[below] {$x$};
	\draw [->] (0, -2) -- (0, 2) node[left] {$y$};

    \draw [ultra thick] (-1, 0) node[below] {$-a$} --++(2, 0) node[below] {$a$};
    \draw [->, thick] (0, 0) --++ (130 : 1) node[above] {$\pmb F$};
    \filldraw (1, 0) circle(1pt) node[above] {$B$};
		
	\draw [->, thick] (-2, -1.5) --++ (40 : 1) node [midway, above] {$V$};
	\draw [dashed] (-2, -1.5) --++ (.7, 0);
	\draw [blue] (-2, -1.5) ++ (.3,0) arc (0 : 40 : .3) node [midway, right] {$\alpha$};
	
	\draw [thick] (1,1.5) circle (0.3) node [] {$z$};
	\draw [thick, <-]  (2.2, 1.3) to[in=150, out = 30]
				node [midway, above] {$z = \zeta + \frac{C^2}{\zeta}$} 		(4.8, 1.3);
				
	\begin{scope}[shift={(7,0)}]
		\draw [->] (-2,0) -- (2,0) node[below] {$\xi$};
		\draw [->] (0,-2) -- (0,2) node[left] {$\eta$};
		\draw [thick] (0,0) circle (.9);
		\draw [thick] (1,1.5) circle (0.3) node [] {$\zeta$};
		\draw [->] (0, 0) -- ({.9*cos(45)}, {-.9*sin(45)}) node[above] {$R$};
        \filldraw (.9, 0) circle(1pt) node[above = 5pt, right] {$B'$};
	\end{scope}
\end{tikzpicture}
\end{center}
Пластина --- эллипс с $b \rightarrow 0$. Тогда $R = \frac{a+b}{2} = \frac{a}{2}$, $C^2 =\frac{a^2 - b^2 }{4} = \frac{a^2}{4}$, т.е. $C = \frac{a}{2}$. Также положим $k = \left.\dif z \zeta\right|_\infty = 1$

Тогда задняя острая кромка в плоскости $z$ перейдет в точку $B'$. Вспомним выражение (20.52).

\[
    \Gamma = 4\pi k R v_\infty \sin\left( \varphi_0 - \alpha \right),
\]

где $\varphi_0$ --- аргумент точки $B'$. В нашем случае $\varphi_0 = 0$. Тогда:

\begin{equation}
    \Gamma = \frac{4\pi a}{2} v_\infty \sin(-\alpha) = - 2\pi a v_\infty  \sin\alpha
\end{equation}

Из выражения (20.48) при $b = 0$ получаем:

\[
\omega(z) = \frac{1}{2} v_\infty e^{-i\alpha}  \left( z + \sqrt{z^2 - a^2}\right) + \frac{1}{2} v_\infty e^{i\alpha} \left( z - \sqrt{z^2 - a^2}\right) + \frac{\Gamma}{2\pi i} \ln\left( z + \sqrt{z^2 - a^2}\right)
\]

Тогда с учетом (20.61) получаем:

\begin{equation}
\omega(z) = \frac{1}{2} v_\infty e^{-i\alpha} \left( z + \sqrt{z^2 - a^2}\right) + \frac{1}{2} v_\infty e^{i\alpha} \left( z - \sqrt{z^2 - a^2}\right) + i a v_\infty \sin\alpha \ln\left( z + \sqrt{z^2 - a^2}\right)
\end{equation}

Также знаем, что $|\pmb F| = \rho v_\infty |\Gamma|$, то есть:

\begin{equation}
	|\pmb F| = 2\pi a \ \rho v_\infty^2 \sin\alpha
\end{equation}

Найдем коэффициент подъемной силы:

\begin{equation}
C_{y_1} = \frac{|\pmb F|}{\frac{1}{2} \rho v_\infty^2 \cdot 2a} = \frac{2\pi a \rho v_\infty^2 \sin\alpha}{\rho v_\infty^2 a } = 2\pi \sin\alpha
\end{equation}

Заметим, что при малых углах атаки $\alpha \ll 1$, коэффициент подъемной силы
\begin{equation}
	C_{y1} \approx 2\pi \alpha
\end{equation}

Ниже приведен график зависимости $C_{y1}(\alpha)$ и экспериментальные данные

\begin{center}
    \begin{tikzpicture}
        \pgfmathsetmacro{\x}{1}
        \pgfmathsetmacro{\y}{2}
        \draw [->] (-2*\x, 0) --++ (4*\x, 0) node[below] {$\alpha$};
        \draw [->] (0, -\y) --++ (0, 2*\y) node[left] {$C_{y1}$};
        \draw (-1*\x, -1*\y) -- (1*\x, 1*\y);
        \foreach \s in {-3, -2, ..., 2, 3}
            \filldraw[blue] ({\s*1/3*\x}, {\s*1/3*\y}) circle(1pt);
        \draw (1*\x, .05*\y) --++ (0, -.1*\y) node[below] {$\sim 10^\circ$};
        \draw (-1*\x, .05*\y) --++ (0, -.1*\y) node[below] {$\sim - 10^\circ$};
        \foreach \s in {1, 1.25, 1.5, 1.75}{
            \filldraw[blue] (\s*\x, {-\s^2*\y + 2.5*\s*\y - .5*\y}) circle (1pt);
            \filldraw[blue] (-\s*\x, {\s^2*\y - 2.5*\s*\y + .5*\y}) circle (1pt);
        }
    \end{tikzpicture}
\end{center} 