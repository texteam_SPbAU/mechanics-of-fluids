\setauthor{М. Минин}
\section{Плоские безвихревые установившиеся течения идеальной несжимаемой жидкости.}
\par Как следует из названия параграфа, будем интересоваться безвихревым плоским течением идеальной несжимаемой жидкости. Интересоваться будем лишь нетеплопроводной жидкостью. Плотность не меняется, значит, в силу того, что $a = \sqrt\dif{p}{\rho}$, скорость звука $a = \infty$. Таким образом, можно считать, что в рассматриваемых задачах мы имеем дело с малыми числами Маха.

\par Допущения для текущего параграфа:

\[
\begin{aligned}
	&v_z = \partd{}{z} = 0 \text{ --- плоское течение}\\
	&\partd{}{t} = 0 \text{ --- установившееся течение}\\
	&\pmb{\Omega} = \Rot \pmb{v} = 0 \text{ --- безвихревое течение}\\
	&\rho = const \text{ --- несжимаемая жидкость}\\
	&\mu = \lambda = 0 \text{ --- нетеплопроводная жидкость}\\
\end{aligned}
\]
\par Рассмотрим все уравнения, которые описывают интересующее нас течение, в рамках принятых допущений. Из уравнения неразрывности для несжимаемой жидкости $\Div \pmb{v} = 0$ и первого допущения следует:

\begin{equation}
	\Div \pmb v = \partd{v_x}{x} + \partd{v_y}{y} = 0
\end{equation}

\par Уравнение Бернулли:

\begin{equation}
	\frac{v_x^2 + v_y^2}{2} + \frac{p}{\rho} + V = const
\end{equation}

\par $V$~--- скалярный потенциал объемных сил $\pmb F = - \nabla V$. В дальнейшем мы будем пренебрегать влиянием объемных сил, поэтому слагаемое с $V$ <<уйдет>> из уравнения.

\par Закон сохранения энергии:
\[
	\rho \dif{U}{t} = \epsilon
\]
\par Здесь $\epsilon$~--- количество тепла, поглощаемого единицей объема в единицу времени. Раскроем производную:
\[
	\dif{U}{t} = \partd{U}{t} + \partd{U}{x} v_x + \partd{U}{y} v_y + \partd{U}{z} v_z = \frac{\epsilon}{\rho}
\]
\par Исходя из наших допущений $\epsilon = 0$, а из слагаемых в левой части остается только два. Значит в итоге закон сохранения энергии запишется как:
\begin{equation}
	v_x \partd{U}{x} + v_y \partd{U}{y} = 0
\end{equation}

\par Из условия безвихреватости для плоского течения получим:

\[
\pmb{\Omega} = \Rot \pmb{v} = {\nabla}\times\pmb{v} =
    \begin{vmatrix}
    \pmb{i} 	& \pmb{j} 		& \pmb{k}\\
    \partd{}{x} & \partd{}{y} 	& 0\\
    v_x 		& v_y 			& 0
    \end{vmatrix}
	=
\pmb{k} \left( \partd{v_y}{x} - \partd{v_x}{y}\right)
\]

\par Таким образом получили еще одно соотношение:

\begin{equation}
	\partd{v_y}{x} - \partd{v_x}{y} = 0
\end{equation}

\par  Получилось $4$ уравнения на $4$ неизвестные функции: $v_x, v_y, p, U$. Обратим внимание, что выражения (20.1) и (20.4) образуют замкнутую подсистему. При этом, эта подсистема верна в нестационарном случае. В дальнейшем мы почти всегда будем в первую очередь интересоваться полем скоростей, то есть решать именно эту подсистему. 


\subsection{Потенциал скоростей.}

\par Поле скоростей потенциально, если  существует функция $\varphi(x,y)$ (\textbf{потенциал скоростей}) такая, что

\[
	\pmb{v} = \Grad \varphi \equiv \nabla \varphi
\]

\par Тогда компоненты скорости соответственно равны:

\begin{equation}
	v_x = \partd{\varphi}{x} \qquad v_y = \partd{\varphi}{y}
\end{equation}

\par Из (20.4) с подстановкой (20.5), если $\varphi$ дважды дифференцируема, получаем, что:

\[
	\partd{}{x} \left( \partd{\varphi}{y} \right) -
	\partd{}{y} \left( \partd{\varphi}{x} \right) = 0
\]

\par То есть если течение безвихревое, то можно считать его потенциальным.

\par Из (20.1) с подстановкой (20.5) получаем, что:

\begin{equation}
	\partd{^2 \varphi}{x^2} + \partd{^2 \varphi}{y^2} = 0
\end{equation}

\par Теперь надо понять как формулируются граничные условия. Рассмотрим задачу обтекания крыла. Если жидкость идеальная, то накладывается условие непротекания и величина скорости на бесконечности.

\[
\begin{aligned}
&\left. \pmb{v} \strut \right|_{\infty} = \pmb{v}_{\infty}\\
&\left. v_n\strut\right|_S = 0
\end{aligned}
\]

\par Первое условие можно переписать через функцию $\varphi$, а условие непротекания преобразуем, воспользовавшись тем, что $v_n = \pmb{v} \cdot \pmb{n} = \Grad \varphi \cdot \pmb{n} = \partd{\varphi}{n}$. Значит граничные условия на функцию $\varphi$ выглядят следующим образом:

\begin{equation}
  \left.\partd{\varphi}{x}\right|_\infty = v_{\infty_x} \quad \left.\partd{\varphi}{y}\right|_\infty = v_{\infty_y} \quad
  \left. \partd{\varphi}{n}\right|_S = 0
\end{equation}

\par Получили внешнюю задачу фон Неймана для уравнения Лапласа (так называется уравнение Лапласа с граничными условиями второго рода).

\subsection{Функция тока.}

\par Условия на компоненты скоростей из (20.1):
\[
	\partd{v_x}{x} = - \partd{v_y}{y}
\]

\par Таким образом можно определить \textbf{функцию тока} $\psi(x,y)$:

\begin{equation}
	\psi\colon \quad v_x = \partd{\psi}{y} \quad v_y = -\partd{\psi}{x}
\end{equation}

\par В (20.4) подставляем (20.8) и получаем:

\begin{equation}
	\partd{^2 \psi}{x^2} + \partd{^2 \psi}{y^2} =0
\end{equation}

\par  Как и в предыдущем пункте, разберемся с граничными условиями. На бесконечности условие такое же, а условие непротекания несколько сложнее. Рассмотрим дифференциал функции $\psi$:

\begin{equation}
	d\psi = \partd{\psi}{x} dx + \partd{\psi}{y} dy = - v_y dx + v_x dy
\end{equation}

\par Из уравнения линии тока: $\dif{y}{x} = \frac{v_y}{v_x}$. Из этого соотношения $v_x dy = v_y dx$. Подставляем это соотношение в (20.10) и получаем, что $d\psi = 0$. Таким образом, вдоль линии тока верно, что:

\begin{equation}
	\psi(x,y) = const
\end{equation}

\par Условие непротекания говорит о том, что поверхность крыла~--- линия тока. Тогда на $\psi$ получаем следующие граничные условия:

\begin{equation}
\left. \partd{\psi}{y}\right|_\infty = v_{\infty_x} \quad \left.  \partd{\psi}{x}\right|_\infty = - v_{\infty_y} \left.\quad
\psi(x, y)\strut \right|_S = 0
\end{equation}

\par Получили внешнюю задачу Дирихле для уравнения Лапласа (так называют уравнение Лапласа с граничными условиями первого рода).

\par Здесь надо сделать пару замечаний:
\begin{enumerate}
\item Потенциал скоростей $\varphi$ связан с уравнением безвихреватости, а функция тока $\psi$ с уравнением неразрывности. Значит функцию тока можно ввести всегда, в то время как потенциал лишь для безвихревого течения.  

\item Линии $\varphi(x, y) = const$ и $\psi(x, y) = const$ взаимноортогоальны.

{\bf Пояснение:} рассмотрим $\Grad \varphi \cdot \Grad \psi$, записав покомпонентно и воспользовавшись (20.5) и (20.8) получаем ноль. Значит $\Grad \varphi$ и $\Grad \psi$ ортогональны, а значит и линии $\varphi = const$ и $\psi = const$ тоже ортогональны

\item Зная функцию $\psi$ несложно вычислить расход жидкости через цилиндрическую поверхность любой формы.
\end{enumerate}

	\begin{wrapfigure}{r}{.25\textwidth}
	\begin{tikzpicture}[scale = 1.2]
		\draw [<->] (0, 2) node [left] {$y$} |- (2, 0) node [below] {$x$};
		\draw (1.5, .5) node [below] {$A$} arc (0 : 90 : 1) node [left] {$B$};
		\draw [->] (.5, .5) ++ (45 : 1) to ++ (45 : .5) node [right] {$\pmb n$};
		\draw [ultra thick] (.5, .5) ++ (45 : 1) ++ (-45 : .2) to ++ (135 : .4) node [below = .1] {$ds$};
		\draw [->, blue, thin] (.3, .2)  to [out = 50, in = 200] ++ (30 : 1.8);
		\draw [->, blue, thin] (.2, .3)  to [out = 40, in = -110] ++ (60 : 1.8);
		\draw [->, blue, thin] (.7, .3)  to [out = 40, in = 180] ++ (20 : 1.3);
		\draw [->, blue, thin] (.3, .7)  to [out = 50, in = -90] ++ (70 : 1.3);
	\end{tikzpicture}
	\end{wrapfigure}
Проделаем это:

\[
    Q_m = \int\limits_A^B \rho v_n\, ds = \rho \int \limits_A^B v_n\, ds =\rho \int\limits_A^B \left[\strut v_x \cdot \cos(n,x) + v_y \cdot \cos(n,y)\right]ds
\]

\par $ds$~--- участок кривой, перпендикулярный нормали, значит $\cos(n, x) = \dif{y}{s}$, а $\cos(n,y) = - \dif{x}{s}$. Тогда:

\begin{equation}
    Q_m = \rho \int\limits_A^B v_x dy - v_y dx = \rho \int\limits_A^B \left( \partd{\psi}{y}dy + \partd{\psi}{x}\right) dx = \rho \int\limits_A^B d \psi = \rho(\psi_B - \psi_A)
\end{equation}

\par Введем понятие объемного расхода:

\begin{equation}
	Q = \frac{Q_m}{\rho} = \psi_B - \psi_A
\end{equation}

\par Можно поставить модуль, чтобы не обращать внимание на знаки

\subsection{Комплексный потенциал и комплексная скорость.}
\par Запишем только что полученные выражения для компонент скорости через потенциал и функцию тока:
\begin{equation}
v_x = \partd{\varphi}{x} = \partd{\psi}{y} \quad
v_y = \partd{\varphi}{y} = -\partd{\psi}{x}
\end{equation}

\par Рассмотрим комплекснозначную функцию $W$:
\begin{equation}
	W(x, y) = \varphi(x,y) + i \psi(x,y)
\end{equation}

\par Будем рассматривать плоскость $(x,y)$ как комплексную плоскость. Перейдем к переменным $z = x + i y$ и $\conj{z} = x - i y$. Обратное преобразование будет выглядеть так:

\[
\left\{
\begin{aligned}
	&x = \frac{z + \conj z}{2}\\
	&y = \frac{z - \conj z}{2i}
\end{aligned}
\right.
\]

\par Тогда функция $W$ в новых переменных записывается следующим образом:

\begin{equation}
W(x, y) = \varphi\left(\frac{z + \conj z}{2}, \frac{z - \conj z}{2i}\right) +
		  i \psi\left(\frac{z + \conj z}{2},\frac{z - \conj z}{2i}\right)
		= \omega \left(z, \conj z\right)
\end{equation}

\par Хотим узнать, при каких условиях $\omega (z,\conj z) = \omega (z)$. Другими словами, хотим, чтобы $\partd{\omega}{\conj z} = 0$. Распишем производную по $\conj z$:

\[
\begin{aligned}
\partd{\omega}{\conj z} &= \partd{\varphi}{x} \partd{x}{\conj z} + \partd{\varphi}{y} \partd{y}{\conj z} + i\left( \partd{\psi}{x} \partd{x}{\conj z} + \partd{\psi}{y} \partd{y}{\conj z} \right) =
\frac{1}{2}\cdot \partd{\varphi}{x} - \frac{1}{2i}\cdot\partd{\varphi}{y} 
+ 
i\left( \frac{1}{2}\cdot\partd{\psi}{x} - \frac{1}{2i}\cdot\partd{\psi}{y}\right) =\\
&=
\frac{1}{2}\left( \partd{\varphi}{x} - \partd{\psi}{y}\right) + \frac{i}{2} \left( \partd{\varphi}{x} + \partd{\psi}{y}\right) = 0
\end{aligned}
\]

\par Получились соотношения, которые мы уже встречали ранее $(20.15)$ и которые выполняются автоматически:

\begin{equation*}
\left\{
\begin{aligned}
	& \partd{\varphi}{x} - \partd{\psi}{y} = 0\\
	& \partd{\varphi}{x} + \partd{\psi}{y} = 0
\end{aligned}
\right. \Longleftrightarrow  (20.15)
\end{equation*}

\par То есть если сможем ввести функцию $\omega$, то получим поле скоростей. $\omega(z) = \varphi + i \psi$~--- \textbf{комплексный потенциал}.

\begin{equation}
\boxed{
	\begin{aligned}
		&\varphi = \Real \omega(z)\\
		&\psi = \Imag \omega(z)
	\end{aligned}
}
\end{equation}

\par Поймем чему равна производная от комплексного потенциала:  
\[ 
	\dif{\omega}{z} = \partd{\varphi}{x} + i \partd{\psi}{x} = v_x + i(-v_y)
\] 
\par Так можно делать, потому что, если есть производная по $z$ в точке $z_0$, то не важно как мы стремимся к точке $z_0$. В данном случае мы решили искать производную, двигаясь по прямой параллельно оси $x$. Таким образом:

\begin{equation}
\dif{\omega}{z} = v_x - i v_i \Rightarrow  \dif{\omega}{z} = \conj v(z)
\end{equation}

Также можно записать:

\begin{equation}
\left\{
\begin{aligned}
	&v_x = \Real \left( \dif{\omega}{z} \right)\\[6pt]
	&v_y = - \Imag \left( \dif{\omega}{z} \right)
\end{aligned}
\right.
\end{equation} 