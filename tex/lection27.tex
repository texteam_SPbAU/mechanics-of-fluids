\setauthor{М. Минин}


$L$ --- окружность Жуковского, $l$ --- профиль Жуковского, а $l'$ --- скелет профиля. Все семейство таких профилей характеризуется параметрами $c$, $\varkappa$ и $\epsilon$.

Была предложена модификация отображения, из-за неудобства формулы Жуковского с точки зрения инженерии:

\[
z = \zeta + \frac{c^2}{\zeta} \quad \Rightarrow \quad \frac{z - 2c}{z+2c} = \left(\frac{\zeta - c}{\zeta +c}\right)^2
\]

И еще одна:

\[
\frac{z - 2c}{z+2c} = \left(\frac{\zeta - c}{\zeta +c}\right)^\sigma, \qquad \sigma = 2 -\frac{\delta}{\pi}
\]

Тогда в задней острой кромке будет угол $\delta$. Это можно понять, внимательно посмотрев на рассуждения, которые приводились при выводе постулата Чаплыгина-Жуковского.

Решим задачу об обтекании. Как уже говорилось ранее, удобнее перейти к параметрам $c$, $\frac{\varkappa}{c}$ и $\frac{\epsilon}{c}$. Физический смысл последних двух: относительный прогиб и относительная толщина контура.

Вспомним задачу об обтекании цилиндра:

\[
\begin{aligned}
W(\zeta_1) &= k \conj v^{\infty} \zeta_1 + k v^{\infty} \frac{R^2}{\zeta_1} + \frac{\Gamma}{2\pi i} \ln \zeta_1\\
k &= \left.\dif{z}{\zeta_1}\right|_\infty, \qquad \Gamma = 4\pi k v_\infty R \sin(\varphi_0 - \alpha) = - 4\pi k v_\infty R \sin(\beta + \alpha)
\end{aligned}
\]

В выражениях выше $\zeta_1$ --- комплексная плоскость $(\xi_1, \eta_1)$. Переход в плоскость $\zeta$  осуществляется следующим образом:

\[
\zeta_1 = \zeta - \left( i\varkappa + \epsilon e^{i(\pi - \beta)} \right) = \zeta - g
\]

Тогда $\zeta = \zeta_1 + g$, а преобразование Жуковского: 
\[
	z = \zeta_1 + g + \frac{c^2}{\zeta_1 + g}
\]
Отсюда следует, что 
\[
	k = \left. \dif{z}{\zeta_1} \right|_\infty = 1
\]

Найдем обратное отображение. Из преобразования Жуковского получаем квадратное уравнение: 
\[
	\zeta^2 - z\zeta + c^2 = 0
\]

Корнем этого уравнения является: 
\[ 
	\zeta = \frac{z+\sqrt{z^2 -4c^2}}{2}
\]
Тогда обратное отображение имеет вид:

\begin{equation}
\zeta_1 = \zeta_g = \frac{1}{2} \left(z + \sqrt{z^2 - 4c^2}\right) - \left( i \varkappa + \epsilon e^{ i(\pi - \beta) }\right)
\end{equation}

Выражение для циркуляции:

\begin{equation}
\Gamma = -4\pi v_\infty \left( \epsilon + \sqrt{c^2 \varkappa^2}\right) \sin(\beta + \alpha)
\end{equation}

Получим комплексный потенциал преобразования Жуковского в координатах $(\xi_1, \eta_1)$:

\begin{multline}
\omega(z) = v_\infty e^{-i\alpha} \left[\frac{1}{2}\left( z + \sqrt{z^2 - 4c^2}\right) - \left(i\varkappa - \epsilon e^{-i\beta} \right)\right] + v_\infty e^{i\alpha} \frac{\left(\epsilon + \sqrt{c^2 + \varkappa}\right)^2}{\left[\frac{1}{2}\left( z + \sqrt{z^2 - 4c^2}\right) - \left(i\varkappa - \epsilon e^{-i\beta} \right)\right]}+\\ + 2iv_\infty \left( \epsilon + \sqrt{c^2 + \varkappa^2}\right)\sin(\alpha + \beta) \ln\left[\frac{1}{2}\left( z + \sqrt{z^2 - 4c^2}\right) - \left(i\varkappa - \epsilon e^{-i\beta} \right)\right]
\end{multline}

Это был большой результат в аэродинамике XX века.

\begin{wrapfigure}{l}{3cm}
\vspace{-10pt}
\begin{tikzpicture}
    \pgfmathsetmacro{\r}{1};
    \pgfmathsetmacro{\C}{.35};
    \pgfmathsetmacro{\X}{1};
    \foreach \R in {0, 1, 2}{
        \draw[rotate=-25] (-\X-\C*\R, -\R*\C) arc(180:90:{\X+\C*\R});
        \draw[rotate=-25] (-\X-\C*\R, -\R*\C) arc(180:{180+(2-\R)*2*\C + (2-\R)*\R*2*\C}:{\X+\C*\R});
    }
    \draw[rotate=-25] ({-\X-2*\C}, -2*\C) to [out =-90, in = -180] ( {-\X - 1.5*\C}, -2.65*\C ) to [out =0, in = -90] (-\X, 0);
    \node[rotate=-15, above] at (0, \X) {$l$};
    \node at (-1.5, .25) {$l'$};
    \draw[<->](-2, -.5) --++ (2.5, 0) node[midway, above] {$b$};
\end{tikzpicture}
\end{wrapfigure}
Прервемся и рассмотрим аэродинамические свойства профиля Жуковского, который является слабоизогнутым ($\frac{\varkappa}{c} \ll 1$), тонким ($\frac{\epsilon}{c} \ll 1$) и обтекается под малым углом атаки ($\alpha \ll 1$)

Прежде всего нас интересует величина подъемной силы из теоремы Жуковского:

\[
|\pmb F| = \rho v_\infty |\Gamma| = \rho v_\infty 4\pi v_\infty \left(\epsilon + \sqrt{c^2 + \varkappa^2} \right) \sin(\alpha+\beta) = 4\pi \rho v_\infty^2 \left( \epsilon + c \sqrt{1 + \frac{\varkappa^2}{c^2}}\right) \sin(\alpha + \beta)
\]

Пользуемся нашими приближениями и получаем:

\begin{equation}
|\pmb F| = 4\pi \rho v_\infty^2 c (\alpha+\beta)
\end{equation}

Также найдем коэффициент подъемной силы. Для этого введем характерную длину контура $b$ (при наших допущениях $b \approx 4 c$):

\begin{equation}
C_{y_1} = \frac{|\pmb F|}{\frac{1}{2}\rho v_\infty^2 b} \approx \frac{4 \pi \rho v_\infty^2 c(\alpha +\beta)}{\frac{1}{2}\rho v_\infty^2 4 c} = 2\pi (\alpha + \beta)
\end{equation}

\begin{wrapfigure}[2]{l}{4cm}
\vspace{-45pt}
\begin{tikzpicture}[scale = 1.2]
    \pgfmathsetmacro{\O}{(1.1^2-.9^2)^(1/2)};
    \pgfmathsetmacro{\Ax}{1.1*cos(60)};
    \pgfmathsetmacro{\Ay}{\O + 1.1*sin(60)};
    \draw [dashed, thick] (-1.5, 0) --++(3.5, 0);
	\draw [thick] 	(-1, 0) .. controls (-1, 1) and (0.8, 0) .. (1.5, 0)
					(-1, 0) .. controls (-1, -1) and (.8, 0) .. (1.5, 0);
\end{tikzpicture}
\end{wrapfigure}
Рассмотрим симметричный профиль с закругленной передней кромкой --- руль Жуковского. Т.к. он симметричный ($\varkappa = 0$), то $\beta = \arctg \frac{\varkappa}{c} = 0$. Тогда:

\begin{equation}
C_{y_1} = 2\pi \alpha
\end{equation}

\par Исчезла проблема передней острой кромки, а сам результат совпадает с результатом, полученным при решении задачи об обтекании плоской пластины.

\par Опять же сравним экспериментальные и теоретические данные. При больших углах атаки появляется отрыв потока от границы контура. Были попытки избавиться от этого отрыва (например, за счет откачивания пограничного слоя).

\begin{center}
\begin{tikzpicture}
    \pgfmathsetmacro{\x}{1}
    \pgfmathsetmacro{\y}{2}
    \draw [->] (-2*\x, 0) --++ (4*\x, 0) node[below] {$\alpha$};
    \draw [->] (0, -\y) --++ (0, 2*\y) node[left] {$C_{y1}$};
    \draw (-1*\x, -1*\y) -- (1*\x, 1*\y);
    \foreach \s in {-3, -2, ..., 2, 3}
        \filldraw[blue] ({\s*1/3*\x}, {\s*1/3*\y}) circle(1pt);
    \draw (1*\x, .05*\y) --++ (0, -.1*\y) node[below] {$\sim 10^\circ$};
    \draw (-1*\x, .05*\y) --++ (0, -.1*\y) node[below] {$\sim - 10^\circ$};
    \foreach \s in {1, 1.25, 1.5, 1.75}{
        \filldraw[blue] (\s*\x, {-\s^2*\y + 2.5*\s*\y - .5*\y}) circle (1pt);
        \filldraw[blue] (-\s*\x, {\s^2*\y - 2.5*\s*\y + .5*\y}) circle (1pt);
    }
\end{tikzpicture}
\end{center}

\begin{wrapfigure}{r}{5cm}
\begin{tikzpicture}[scale = 1.4]
    \pgfmathsetmacro{\O}{(1.1^2-.9^2)^(1/2)};
    \pgfmathsetmacro{\Ax}{1.1*cos(60)};
    \pgfmathsetmacro{\Ay}{\O + 1.1*sin(60)};
    \draw [thick] (-2, 0) --++(1, 0);
    \draw [dashed, thick] (-1, 0) --(1.5, 0);
	\draw [thick] 	(-1, 0) .. controls (-1, 1) and (0.8, 0) .. (1.5, 0)
					(-1, 0) .. controls (-1, -1) and (.8, 0) .. (1.5, 0);
    \filldraw (-1, 0) circle(1pt);
    \draw(-1.2, 0) node[below] {$p_0$};
    \draw[thick, <->] (-1, -1.1) -- (1.5, -1.1) node[midway, below] {$b$};
    \draw[thick, <->] (1.7, .8) -- (1.7, -.8) node[midway, right] {$t$};
\end{tikzpicture}
\end{wrapfigure}
Обычно в экспериментах сила и давление измеряются очень хорошо. Давайте рассмотрим коэффициент давления в передней точке для симметричного профиля Жуковского. Выражение для коэффициента давления следующее:

\[
c_p = \frac{p - p_\infty}{\frac{1}{2}\rho v_\infty^2}
\]

Запишем закон Бернулли:

\[
\frac{v^2}{2} + \frac{p}{\rho} = \frac{p_0}{\rho},
\]

где $p_0$ --- давление торможения (скорость торможения равна нулю). Из закона Бернулли сразу получаем, что коэффициент давления в передней кромке равен $c_{p_0} = 1$

Рассмотрим распределение давление. Самое удивительное, что это очень хорошо согласуется с экспериментом. На графике построили теоретические кривые для контуров, для которых $t/b = 9\%$ и $15\%$

\begin{center}
	\begin{tikzpicture}
	\begin{axis}[
		axis x line=bottom,
		axis y line=left,
		xmin=0, xmax=1.2,
		ymin=-1.5, ymax=1.2,
		xlabel = $x/b$,
		ylabel = $c_p$,
		grid=both,
		legend pos=north west,
		width=.5*\textwidth,
	]
    \addplot[color=red!20, very thick, domain = 0 : 1, samples=150]
    {
    4*(x-1)*(x-.25)
     };
     \addplot[color=red, loosely dotted, ultra thick, domain = 0 : 1, samples=150]
    {
    4*(x-1)*(x-.25)
     };
     \addplot[color=blue!20, very thick, domain = 0 : 1, samples=150]
    {
    100/15*(x-1)*(x-.15)
     };
     \addplot[color=blue, loosely dotted, ultra thick, domain = 0 : 1, samples=150]
    {
    100/15*(x-1)*(x-.15)
     };
	\end{axis}
    \node[blue] at (5.5, 1) {$9\%$};
	\end{tikzpicture}
\end{center}

Рассмотрим преобразование в профиль Жуковского, которое не имеет аналитического обратного:

\[
\left\{
\begin{aligned}
&W(\zeta_1) = k\conj v^{\infty} \zeta_1 + k v^{\infty} \frac{R^2}{\zeta_1} + \frac{\Gamma}{2\pi i } \ln\zeta_1\\
&z = f(\zeta_1)
\end{aligned}
\right.
\]

Если обратного нет, то можно считать, что $\omega(z)$ задан параметрически: $\zeta_1 = R e^{i\theta}$
