\clearpage
\setauthor{И. Буренев}
\section{Закон изменения момента количества движения.}
\setcounter{equation}{0}

\subsection*{Орбитальный момент.}
\begin{wrapfigure}[4]{l}{4cm}
	\vspace{-35pt}
	\begin{center}
	\begin{tikzpicture}[scale=1.2, thick]
	\pgfmathsetmacro{\r}{1};
	\draw (0,0) circle (\r) (100:1.2*\r)node[above,left]{$\tau$};
	\draw[->] (-.6*\r,-1.6*\r)coordinate(O)node[below]{$O$} to (.5*\r, .4*\r) coordinate(T)node[midway]{$\pmb{r}$};
	\filldraw(T) circle (.06*\r)node[above,left=.1]{$d\tau$};
	\draw[->] (T)to++(-40:1.2*\r)node[right]{$d\pmb{R}$};
	\draw[->](T)to++(\r,0)node[above ]{$v$};
	\end{tikzpicture}
	\end{center}
	\vspace{20pt}
\end{wrapfigure}

\par Рассмотрим жидкий фрагмент $\tau$, на который действует объемная сила $d\pmb{R}$
\begin{align}
\nonumber d\tau&:\quad d\pmb{K} = dm\,\pmb{v}=\rho\pmb{v}\,d\tau\\
\nonumber d\tau&:\quad d\pmb{L} = \pmb{r}\times d\pmb{K}=
								 \rho \pmb{r}\times\pmb{v}\,d\tau\\	
\tau&:\quad L=\iiint\limits_{\tau}\pmb{r}\times\pmb{v}\rho\, d\tau
\end{align}


\subsection*{Замечание о внутреннем моменте.}
\par При некоторых условиях у элементарной частицы $d\tau$ может быть момент, обусловленный внутренним вращением этой частички. В обычных газах (жидкостях) внутренний момент равен нулю (с точностью до молекулярных флуктуаций) в виду хаотического вращения и хаотической ориентации молекул. В некоторых особых случаях (магнитная гидродинамика, механика суспензий) внутренний момент может быть отличен от нуля, но рассмотрение этих случаев выходит за рамки курса.

\subsection*{Закон изменения момента количества движения.}

\par Если в потоке отсутствуют пространственно распределенные пары сил, то скорость изменения момента количества движения выделенной массы равна главному моменту всех внешних сил, приложенных к этой массе.\vspace{-24pt}
\begin{wrapfigure}[5]{l}{4cm}
	\vspace{0pt}
	\begin{center}
		\begin{tikzpicture}[scale=1.2, thick]
		\pgfmathsetmacro{\r}{1};
		\draw (0,0) circle (\r);
		\path (100:1.2*\r)node[above,left]{$\tau$};
		\draw[->] (-.6*\r,-1.6*\r)coordinate(O)node[below]{$O$} to(.5*\r,.4*\r) coordinate(T)node[midway]{$\pmb{r}$};
		\filldraw(T) circle (.06*\r) node [above, left = .1] {$d\tau \; dS\;$};
		\draw[->] (T)to++(-40:1.2*\r)node[right]{$d\pmb{R}$};
		\draw[->] (T)to++(45:.8*\r)node[right]{$d\pmb{p}$};
		\draw[->](T)to++(\r,0)node[above ]{$v$};
		\end{tikzpicture}
		\label{fig:lec3_main}
	\end{center}
\end{wrapfigure}

\begin{align}
\nonumber
	d\tau &:\quad d\pmb{M}_\tau=\pmb{r}\times d\pmb{R}=\pmb{r}\times\rho\pmb{F}\,d\tau\\
	\tau &:\quad
	\pmb{M}_\tau=\iiint\limits_\tau(\pmb{r}\times\rho\pmb{F})\,d\tau\\
\nonumber\\
\nonumber
	d\tau &:\quad d\pmb{M}_s=\pmb{r}\times d\pmb{p}=(\pmb{r}\times\pmb{\pi}_n)\,dS\\
	\tau &:\quad \pmb{M}_s=\iint\limits_S(\pmb{r}\times\pmb{\pi}_n)\,dS
\end{align}

Можем записать, что:

\[
\pdt{\pmb L} = \pmb M = \pmb{M}_\tau + \pmb{M}_s
\]

\par Тогда сформулированный закон можно переписать в интегральной форме как:
\begin{equation}\label{eq:lec3_3-4integrate}
	\pdt{}\iiint\limits_\tau(\pmb{r}\times\rho\pmb{v})d\tau
	=	\iiint\limits_\tau(\pmb{r}\times\rho\pmb{F})d\tau
	+	\iint\limits_S(\pmb{r}\times\pmb{\pi}_n)dS
\end{equation}

\par Теперь будем последовательно преобразовывать слагаемые, чтобы получить дифференциальную форму.
$$\pdt{}\iiint\limits_\tau(\pmb{r}\times\rho\pmb{v})\,d\tau \stackrel{(****)}{=}
\iiint\limits_\tau\left(
		\pdt{}(\pmb{r}\times\rho\pmb{v})  +
	  	(\pmb{r}\times\rho\pmb{v})\Div\pmb{v}
		\right)d\tau =
\iiint\limits_\tau\left( \vphantom{\dif{}{} }\right. 	
					\pmb{r}\times\rho\pdt{}\pmb{v} +
					\pmb{r}\times\pmb{v}\underbrace{\left(\pdt{\rho}+\rho\Div\pmb{v}  \right)}_{=0}
				\left. \vphantom{\dif{}{} } \right)d\tau $$

\par Значит левую часть можно переписать как:
\begin{equation}
	\pdt{}\iiint\limits_\tau(\pmb{r}\times\rho\pmb{v})\,d\tau  = \iiint\limits_\tau\left( 	\pmb{r}\times\rho\pdt{}\pmb{v}\right)d\tau
\end{equation}

\par Осталось переписать интеграл по поверхности в интеграл по объему. Применим к нему теорему Остроградского-Гаусса.
\begin{align}
\nonumber\iint\limits_S(\pmb{r}\times\pmb{\pi}_n)\,dS
	&=
	\iint\limits_S\left(
		 \pmb{r}\times\pmb{\pi}_x\cos(n,x)
		+\pmb{r}\times\pmb{\pi}_y\cos(n,y)
		+\pmb{r}\times\pmb{\pi}_z\cos(n,z)\right)dS=\\
	&=	\iiint\limits_\tau\pmb{r}\times
	\left(\pdx{\pmb{\pi}_x}+\pdx[y]{\pmb{\pi}_y}+\pdx[z]{\pmb{\pi}_z} \right)d\tau
	+ 	\iiint\limits_\tau	\underbrace{\pdx[x]{\pmb{r}}}_{\pmb i}\times\pmb{\pi}_x+
					\underbrace{\pdx[y]{\pmb{r}}}_{\pmb j}\times\pmb{\pi}_y+
					\underbrace{\pdx[z]{\pmb{r}}}_{\pmb k}\times\pmb{\pi}_z d\tau
\end{align}

\par Теперь подставляя полученные выражения в формулу (\ref{eq:lec3_3-4integrate}) получаем:
$$\pmb{r}\times\left(\vphantom{\dif{}{} } \right.
		\underbrace{\rho\pdt{}\pmb{v}-\rho\pmb{F} -
		\left(\pdx{\pmb{\pi}_x}+\pdx[y]{\pmb{\pi}_y}+\pdx[z]{\pmb{\pi}_z} \right)}_{=0\;(\ref{eq:lec2_2-14})}
		 \left. \vphantom{\dif{}{} } \right)
= \pmb i\times\pmb{\pi}_x + \pmb j\times\pmb{\pi}_y + \pmb k\times\pmb{\pi}_z$$

\par Таким образом получили следующие соотношение для тензора напряженности:
\begin{equation}
		  (\pmb i\times\pmb{\pi}_x)
		 +(\pmb j\times\pmb{\pi}_y)
		 +(\pmb k\times\pmb{\pi}_z) = 0
\end{equation}

\par Для лучшего понимания распишем $\pmb{\pi}_{x,y,z}$ по базису $\pmb i, \pmb j, \pmb k$
$$\begin{cases}
	\pmb i\times\pmb{\pi}_x = \pi_{xy}\, \pmb k - \pi_{xz}\, \pmb j\\
	\pmb j\times\pmb{\pi}_y = \pi_{yz}\, \pmb i - \pi_{yx}\, \pmb k\\
	\pmb k\times\pmb{\pi}_z = \pi_{zx}\, \pmb j - \pi_{zy}\, \pmb i\\
\end{cases}$$

\par Тогда
$$(\pmb i\times\pmb{\pi}_x) + (\pmb j\times\pmb{\pi}_y) + (\pmb k\times\pmb{\pi}_z)=
   \pmb i(\pi_{yz}-\pi_{zy}) + \pmb j(\pi_{zx}-\pi_{xz}) + \pmb k(\pi_{xy}-\pi_{yx})=0$$

\par Вектор равен нулю, значит все его компоненты равны нулю.
\begin{equation}
\begin{matrix}
	\pi_{xy}=\pi_{yx}\\
			\pi_{yz}=\pi_{zy}\\
	\pi_{xz}=\pi_{zx}
\end{matrix}
\end{equation}
\par Дифференциальную форму мы не получили, зато показали, что тензор напряжения симметричен.


\section{Закон сохранения и превращения энергии.}
\par Мы считаем, что частичка в каждый момент времени находится в термодинамическом равновесии.
\par Введем удельную внутреннюю энергию жидкости $U$. $([U]=\text{Дж/кг})$. С точки зрения физики в $U$ следует включить следующие факторы:
\begin{enumerate}[noitemsep,topsep=0pt]
	\item Кинетическую энергию поступательного движения молекул.
	\item Кинетическую энергию вращательного движения молекул.
	\item Энергию колебаний атомов в молекуле.
	\item Энергию связи атомов (<<Энергия диссоциации>>).
	\item Энергию связи электронов с ядрами (<<Энергия ионизации>>).
	\item Энергию связи элементарных частиц в ядрах (<<Ядерная энергия>>).
	\item Энергию потенциального взаимодействия молекул друг с другом и с окружающими телами.
\end{enumerate}
\par Однако в курсе МЖГ логично рассматривать компоненты, которые изменяются при макроскопическом движении. Поэтому обычно включается 1, 2 и иногда 3 составляющая.
\par Рассмотри в качестве примера идеальный газ. Из курса общей физики известно:
	\begin{align*}
	\text{Для одноатомного}&:\quad U=U_\text{пост}=\frac{3}{2}R_gT\\
	\text{Для двухатомного}&:\quad U=U_\text{пост}+U_\text{вр}=\frac{5}{2}R_gT,
	\end{align*}

где $R_g = \frac{R}{Mr}$. $R$ ~--- универсальная газовая постоянная, а $Mr$ ~--- молярная масса конкретного газа

\par Эти формулы работают, если температура $150K<T<(600-700)~K$. Потом становится заметным вклад колебательной энергии.
\begin{equation}
	U=\frac{5}{2}R_gT+\int\limits_0^Tc_{v_\text{кол}}dT
	=\int\limits_0^T\underbrace{\left(\frac{5}{2}R+c_{v_\text{кол}}\right)}_{c_v}dT= \int\limits_0^Tc_vdT
\end{equation}
\par Рассмотрим элементарную энергию частицы.
\begin{align}
	\nonumber	d\tau&: \quad dE_\text{вн} = dm\,U=\rho U\,d\tau\\
				\tau&: \quad E_\text{вн} = \iiint\limits_\tau\rho U\,d\tau
\end{align}

\par Если есть макродвижение надо добавить кинетическую энергию:
\begin{align}
\nonumber	d\tau&: \quad dE_\text{кин} = dm\,\frac{v^2}{2}=\rho\frac{v^2}{2}d\tau\\
			\tau&: \quad E_\text{кин} = \iiint\limits_\tau\rho\frac{v^2}{2}\,d\tau
\end{align}

\par А значит полная энергия:
\begin{equation}
	E=E_\text{вн}+E_\text{кин}=\iiint\limits_\tau\rho\left(U+\frac{v^2}{2}\right)d\tau
\end{equation}

\par При движении жидкой частички ее полная энергия будет изменятся. Причинами этого изменения будут:
\begin{enumerate}[noitemsep,topsep=0pt]
	\item Работа объемных сил.
	\item Работа поверхностных сил.
	\item Поглощение тепла объемом.
	\item Поглощение тепла через поверхность
\end{enumerate}
\par Рассмотрим каждую из причин более подробно.

\paragraph*{Работа объемных сил.}
\begin{align}
	\nonumber  d\tau&:\qquad d\pmb{R}\cdot\pmb{v}\,dt=\rho\pmb{F}\cdot\pmb{v}\,dtd\tau\\
	\nonumber  	\tau&:\qquad dt\iiint\limits_\tau\rho\pmb{F}\cdot\pmb{v}\,d\tau\\
	\text{Мощность}&: 	\qquad A_\tau=\iiint\limits_\tau\rho\pmb{F}\cdot\pmb{v}\,d\tau
\end{align}

\paragraph*{Работа поверхностных сил.}
\begin{align}
	\nonumber  dS&:\qquad d\pmb{p}\cdot d\pmb{r}=\pmb{\pi}_n\cdot\pmb{v}\,dtdS\\
	\nonumber  	S&:\qquad dt\iint\limits_S\pmb{\pi}_n\cdot\pmb{v}\,dS\\
	\text{Мощность}&: \qquad A_S=\iint\limits_S\pmb{\pi}_n\cdot\pmb{v}\,dS
\end{align}

\paragraph*{Поглощение тепла объемом.}
\par Пусть $\eps$~--- количество тепла, поглощаемое единицей объема в единицу времени.
\begin{align}
	\nonumber  d\tau&:\qquad \eps\, d\tau dt\\
	\nonumber  	\tau&:\qquad dt\iiint\limits_\tau\eps\, d\tau\\
	\text{Мощность}&: \qquad Q_\tau=\iiint\limits_\tau\eps \,d\tau
\end{align}

\paragraph*{Поглощение тепла через поверхность.} Пусть $\pmb{q}$~--- вектор плотности потока тепла.\\\vspace{-48pt}
\begin{wrapfigure}[5]{r}[20pt]{5cm}
	\vspace{10pt}
	\begin{center}
		\begin{tikzpicture}[scale=1, thick]
		\pgfmathsetmacro{\r}{1};
		\draw (0,0) circle (\r);
		\path (100:1.2*\r)node[above,left]{$\tau$};
		\path (.5*\r,.4*\r) coordinate(T);
		\filldraw(T)circle(.08*\r)node[above,left=.1]{$dS$};
		\draw[->] (T)to++(45:.8*\r)node[right]{$d\pmb{n}$};
		\draw[->](T)to++(1.2*\r,0)node[below]{$q$};
		\end{tikzpicture}
	\end{center}	
\end{wrapfigure}

\begin{align}
\nonumber  dS&:\qquad -\pmb{q}\cdot\pmb{n}\,dtdS\\
\nonumber  	S&:\qquad -dt\iint\limits_S\pmb{q}\cdot\pmb{n}\,dS\\
\text{Мощность}&: \qquad Q_S=-\iint\limits_S\pmb{q}\cdot\pmb{n}\,dS
\end{align}

\par Теперь запишем закон сохранения и превращения энергии для частички жидкости $\tau$.


\begin{equation}
	\pdt{E}= A_\tau + A_S + Q_\tau + Q_S
\end{equation}

\par Теперь подставим сюда полученные соотношения:
\begin{equation}\label{eq:lec3_4-10integrate}
	 \pdt{}\iiint\limits_\tau\rho\left(U+\frac{v^2}{2}\right)d\tau=
	  \iiint \limits_\tau\rho\pmb{F}\cdot\pmb{v}\,d\tau
	  +\iint \limits_S\pmb{\pi}_n\cdot\pmb{v}\,dS
	  +\iiint \limits_\tau\eps \,d\tau
	  -\iint \limits_S\pmb{q}\cdot\pmb{n}\,dS   	
\end{equation}

\par Чтобы получить дифференциальную форму надо переписать интегралы по поверхности в интегралы по объему и занести производную по времени внутрь.
\begin{align}
	\nonumber \pdt{}\iiint\limits_\tau\rho\left(U+\frac{v^2}{2}\right)\,d\tau
		\stackrel{(****)}{=}
		&
		\iiint\limits_\tau  \left( \pdt{} \rho\left(U+\frac{v^2}{2}\right)
							+\rho\left(U+\frac{v^2}{2}\right)\Div\pmb{v}\right)\,d\tau=\\
	\nonumber
		=&\iiint\limits_\tau\left( \vphantom{\dif{}{} } \right.
		\rho\pdt{}\left(U+\frac{v^2}{2} \right)
		+\left(U+\frac{v^2}{2}\right)
		\underbrace{\left(\pdt{\rho}+\rho \Div\pmb{v}\right)}_{=0}
		\left. \vphantom{\dif{}{} } \right)d\tau=\\
		=&\iiint\limits_\tau\rho\pdt{U}\,d\tau +
			\iiint\limits_\tau\rho \pmb{v}\;\pdt{\pmb{v}}\,d\tau
\end{align}

\begin{align}
	\nonumber
	\iint\limits_S\pmb{\pi}_n\cdot\pmb{v}\,dS=
	&=\iint\limits_S \left(\strut
			 \pmb{v}\;\pmb{\pi}_x\cos(n,x)
			+\pmb{v}\;\pmb{\pi}_y\cos(n,y)
			+\pmb{v}\;\pmb{\pi}_z\cos(n,z)\right)dS\\
	&=\iiint\limits_\tau\pmb{v}
		\left(\pdx[x]{\pmb{\pi}_x}+\pdx[y]{\pmb{\pi}_y}+\pdx[z]{\pmb{\pi}_z} \right)d\tau
	+\iiint\limits_\tau\left(
			 \pmb{\pi}_x\pdx[x]{\pmb{v}}
			+\pmb{\pi}_y\pdx[y]{\pmb{v}}
			+\pmb{\pi}_z\pdx[z]{\pmb{v}}\right)d\tau
\end{align}

\begin{align}
	\iint\limits_S \pmb{q}\;\pmb{n}\;dS=\iiint\limits_\tau\Div\pmb{q}\,d\tau
\end{align}
\par Подставим все в (\ref{eq:lec3_4-10integrate}) и вынесем все слагаемые с $\pmb{v}$ в одну сторону.
$$
	\iiint\limits_\tau\pmb{v}\underbrace{\left(
					\rho\pdt{\pmb{v}} -\rho\pmb{F}- \left(\pdx[x]{\pmb{\pi}_x}+\pdx[y]{\pmb{\pi}_y}+\pdx[z]{\pmb{\pi}_z} \right)\right)}_{=0\;(\ref{eq:lec2_2-14})}d\tau
	+\iiint\limits_\tau\left( \rho\pdt{U} - \left( \pmb{\pi}_x\pdx[x]{\pmb{v}}+\pmb{\pi}_y\pdx[y]{\pmb{v}}+\pmb{\pi}_z\pdx[z]{\pmb{v}}\right) + \Div\pmb{q}-\eps \right)d\tau=0
$$
\par Или в дифференциальной форме записи:
\begin{equation}
	\rho\pdt{U} =	\pmb{\pi}_x\pdx[x]{\pmb{v}}+\pmb{\pi}_y\pdx[y]{\pmb{v}}+\pmb{\pi}_z\pdx[z]{\pmb{v}} -\Div\pmb{q}+\eps
\end{equation}
\par Заметим, что в этом уравнении нет кинетической энергии и внешних сил. И это не случайность! Закон изменения удельной кинетической энергии следует из закона сохранения импульса (\ref{eq:lec2_2-14}). Запишем его:
\begin{equation}
	\rho\pdt{}\frac{v^2}{2}=\rho\pmb{F}\;\pmb{v} + \pmb{v}\left(\pdx[x]{\pmb{\pi}_x}+\pdx[y]{\pmb{\pi}_y}+\pdx[z]{\pmb{\pi}_z}\right)
\end{equation}

\clearpage

\section{Поле скоростей сплошной среды в окрестности точки. Тензор скоростей деформации.}
\subsection*{Вектор завихренности.}
\par Введем вектор завихренности поля скоростей $\pmb{\Omega}=\Rot\pmb{v}=\nabla\times\pmb{v}$
\begin{equation}
	\left\{
	\begin{aligned}
	\pmb{\Omega}_x &= \pdx[y]{v_z}-\pdx[z]{v_y}\\
	\pmb{\Omega}_y &= \pdx[z]{v_x}-\pdx[x]{v_z}\\
	\pmb{\Omega}_z &= \pdx[x]{v_y}-\pdx[y]{v_x}\\
	\end{aligned}\right.
\end{equation}

\subsection*{Связь вектора $\pmb{\Omega}$ с угловой скоростью.}

\par Пусть жидкая частичка вращается с мгновенной угловой скоростью $\pmb{\omega}$ относительно оси, проходящей через полюс~$O$.

\begin{wrapfigure}[5]{l}{5cm}	
	\vspace{-10pt}
	\begin{center}
		\begin{tikzpicture}[scale=1, thick]
		\pgfmathsetmacro{\r}{1};
		\draw (0,0) circle (\r);
		\path (100:1.2*\r)node[above,left]{$\tau$};
		\filldraw (-.5*\r,-.4*\r) coordinate(O)node[below]{$O$} circle(.04*\r);
		\draw[->] (O)to++(65:2*\r)coordinate(W)to++(65:.5*\r);
		\draw[marrow, thin] ($(W) + (0, .1*\r)$) arc (90:450: .3 cm and .1 cm) node[right, pos = .6] {$\pmb{\omega}$};
		\draw[->] (O) --++ (10:1.2*\r) coordinate(R) node[pos=.8, below]{$\pmb{r}$};
		\draw[->] (R) --++(110:.5*\r)node[pos=.8, right]{$\pmb{v}$};
		\end{tikzpicture}		
	\end{center}
\end{wrapfigure}
$$\pmb{v}=\pmb{\omega}\times\pmb{r}=
	\left({\omega_y}z - {\omega_z}y\right)\pmb i+
	\left({\omega_z}x - {\omega_x}z\right)\pmb j+
	\left({\omega_x}y - {\omega_y}x\right)\pmb k$$
\par Выпишем компоненты скорости $v$.
\begin{equation}
\left\{\begin{aligned}
	v_x &= {\omega_y}z - {\omega_z}y\\
	v_y &= {\omega_z}x - {\omega_x}z\\
	v_z &= {\omega_x}y - {\omega_y}x\\
\end{aligned}\right.\stackrel{\pd_{x,y,z}}{\Longrightarrow}
\left\{\begin{aligned}
	\omega_x&= \frac{1}{2}\left(\pdx[y]{v_z}-\pdx[z]{v_y}\right)\\
	\omega_y&= \frac{1}{2}\left(\pdx[z]{v_x}-\pdx[x]{v_z}\right)\\
	\omega_z&= \frac{1}{2}\left(\pdx[x]{v_y}-\pdx[y]{v_x}\right)\\
\end{aligned}\right.
\end{equation}

\par Таким образом получаем соотношение между $\pmb{\Omega}$ и $\pmb\omega$
\begin{equation}
	\pmb{\Omega}=2\pmb{\omega}
\end{equation}

\subsection*{Теорема Гельмгольца.}
\begin{wrapfigure}[5]{l}{5cm}
	\begin{center}
		\vspace{-12pt}
		\begin{tikzpicture}[scale=1,thick]
		\pgfmathsetmacro{\r}{1};
		\pgfmathsetmacro{\dr}{.8*\r};
		\draw[->] (\r,\r) coordinate(M) -- ($(M)+(0:\dr)$) coordinate(N) node[pos=.5,below]{$\delta\pmb{r}$};
		\filldraw(M)circle(.03)node[above]{$M$};
		\filldraw(N)circle(.03)node[above]{$N$};
		\draw[->](0,0)coordinate(O)node[below]{$O$}--(M)node[pos=0.3,above]{$\pmb{r}$};
		\draw ($(M)!.5!(N)$) ellipse (1.4*\dr cm and .6*\dr cm);
		\draw[marrow] ($(O)!.4!(M)$) arc (45:-45:.4*\r)node[pos=.4,right]{$\pmb{\omega}$};
		\end{tikzpicture}	
	\end{center}
\end{wrapfigure}
\par Хотим получить что-то вида $\pmb v=\pmb v_\text{пост}+\pmb v_\text{вр}$ как было в классической механике. Для этого рассмотрим частичку, достаточно малую  для того, чтобы считать $\pmb{\omega}=const$. В этой частичке выделим две точки $M$ и $N$.
$$\pmb{v}_N=\pmb{v}(\pmb{r}+\delta\pmb{r})=\pmb{v}(\pmb{r}) + \pdx[\pmb{r}]{\pmb{v}}\delta\pmb{r}$$


\par Рассмотрим это в проекции на ось $x$.
\begin{align*}
	&v_x(x+\delta x,y+\delta y, z+\delta z ) = v_x +\left(\pdx[x]{v_x}\delta x+\pdx[y]{v_x}\delta y+\pdx[z]{v_x}\delta z \right) =\\
	&=v_x +\pdx[x]{v_x}\delta x
		+\frac{1}{2}\left(\pdx[y]{v_x} + \pdx[x]{v_y} \right)\delta y
		+\frac{1}{2}\left(\pdx[y]{v_x} - \pdx[x]{v_y} \right)\delta y
		+\frac{1}{2}\left(\pdx[z]{v_x} + \pdx[x]{v_z} \right)\delta z
		+\frac{1}{2}\left(\pdx[z]{v_x} - \pdx[x]{v_z} \right)\delta z=\\
	&=v_x+(\omega_y\delta z - \omega_z\delta y)+
	\eps_{xx}\delta x+\eps_{xy}\delta y +\eps_{xz}\delta z
\end{align*}
\par Здесь $\eps_{\alpha\beta}=\frac{1}{2}\left(\pdx[\alpha]{v_\beta}+\pdx[\beta]{v_\alpha}  \right)\quad \alpha,\beta = x,y,z$
\par Тогда компоненты скорости в первом приближении:
\begin{align}
	v_x(\pmb{r}+\delta\pmb{r}) &=v_x + (\pmb{\omega}\times\delta\pmb{r})_x
		+\eps_{xx}\delta x+\eps_{xy}\delta y +\eps_{xz}\delta z\\
	v_y(\pmb{r}+\delta\pmb{r}) &=v_y + (\pmb{\omega}\times\delta\pmb{r})_y
		+\eps_{yx}\delta x+\eps_{yy}\delta y +\eps_{yz}\delta z\\
	v_z(\pmb{r}+\delta\pmb{r}) &=v_z + (\pmb{\omega}\times\delta\pmb{r})_z
		+\eps_{zx}\delta x+\eps_{zy}\delta y +\eps_{zz}\delta z	
\end{align}
\par Запишем получившуюся систему в векторном виде. Для этого определим матрицу $E$. \begin{equation}
	\hat{E}_{\alpha,\beta}=\eps_{\alpha\beta}
\end{equation}
\begin{equation}
	\pmb{v}\;(\pmb{r}+\delta\pmb{r})=\pmb{v}(\pmb{r}) + \pmb \omega\times\delta\pmb{r} +\hat{E}\delta\pmb{r}
\end{equation}
\par Полученная формула представляет из себя {\bfseries теорему Гельмгольца}: Если задано непрерывно дифференцируемое поле скоростей сплошной среды, то в малой окрестности произвольной материальной точки $M$ скорость любой другой материальной точки $N$ можно представить как суперпозицию скорости полюса $M$, скорости точки $N$ относительно полюса $M$ из-за вращательного движения жидкости с мгновенной угловой скоростью $\pmb{\omega}=\frac{1}{2}\Rot\pmb{v}$ относительно мгновенной оси, проходящей через полюс $M$, и скорости точки $N$ относительно $M$ из-за деформационного движения жидкости (последнее слагаемое).
\begin{equation}
	\pmb{v}_\text{деф} = \hat{E}\delta\pmb{r}
\end{equation}
\par Тензор $\hat{E}$ задает линейный оператор, который отображает вектор $\delta\pmb{r}$ в вектор $\pmb{v}_\text{деф}$.  $\hat{E}$ называют {\bfseries тензором скоростей деформации}. Очевидно, что этот тензор симметричен, но на этом его замечательные свойства не заканчиваются.
\begin{equation}
	\Tr\hat{E}=\pdx[x]{v_x}+\pdx[y]{v_y}+\pdx[z]{v_z}=\Div\pmb{v}
\end{equation}





