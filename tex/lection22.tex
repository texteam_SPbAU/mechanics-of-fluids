\clearpage

\setauthor{И. Буренев}
\subsection{Поперечное обтекание цилиндра однородным на бесконечности потоком.}

\begin{wrapfigure}{L}{5cm}
	\begin{tikzpicture}
		\draw [pattern = north west lines, pattern color = black!30] (0,0) circle (.8);
		\draw [<->] (0,1.8) node [left] {$y$} |- (1.8,0) node [below] {$x$};
		\draw [->, thick] (0,0) --++ (60 : 1.5) node [right] {$\pmb U$};
		\draw [blue, ->, thick] (0,0) --++ (-160:.8) node [midway, below] {$R$};
		\draw [->, thick] (-2.5,-1) --++ (40:1) node [midway, above] {$V$};
	\end{tikzpicture}
\end{wrapfigure}

\par Будем искать комплексный потенциал для поперечного обтекания цилиндра потоком, однородным на бесконечности.
\[
	\conj{v} =\dif{ \omega }{ z } = v_x - i v_y
\]
\par Скорость должна быть ограничена на бесконечности, значит ее ряд Лорана не содержит положительных степеней $z$:
\[
	\conj{ v } (z) = c_0 +  \frac{ c }{ z } + \frac{ c_1' }{ z^2 } + \ldots
\]
\par Из граничного условия на бесконечности:
\[
	V_x - i V_y = c_0
\]
\par Подставим этот ряд в комплексный потенциал и проинтегрируем его.
\begin{equation}
	\omega (z) = \left( V_x - i V_y \right) z + C \ln z + \sum_{ 1 }^{ \infty } \frac{ C_n }{ z^n }
\end{equation}


\begin{wrapfigure}{r}{5cm}
	\begin{tikzpicture}
		\draw [pattern = north west lines, pattern color = black!30] (0,0) circle (.8);
		\draw [<->] (0,1.8) node [left] {$y$} |- (1.8,0) node [below] {$x$};
		\draw [blue] (1,0) arc (0:40:1) node [midway, right] {$\theta$};
		\draw [->, thick] (40:.8) --++ (60 : 1.5) node [left] {$\pmb U$} coordinate (U);
		\draw [dashed] (0,0) --++ (40:.8) ++ (40:{1.5*cos(20)}) -- (U);
		\draw [->, thick] (40:.8) --++ (40:{1.5*cos(20)}) node [right] {$U_n$};
	\end{tikzpicture}
\end{wrapfigure}
\par Теперь поставим граничные условия на цилиндре. В полярных координатах:
\[
	\pmb v = \Grad \varphi \Rightarrow v_n = \Grad \varphi \cdot \pmb n = \partd{ \varphi }{ n }
	=\partd{ \varphi }{ r }
\]
\par Условия непротекания:
\[
	v_n = \left.\strut U_n \right|_{r=R}
\]
принимает следующий вид (воспользуемся тем, что $\pmb n = (\cos\theta, \sin\theta)$):
\begin{equation}
	\left. \partd{ \varphi }{ r } \right|_{r=R} = U_n = \pmb U \cdot \pmb n
	=  U_x \cos \theta + U_y \sin \theta
\end{equation}

\par Теперь подставим граничное условие в ряд Лорана. Будем считать, что  $ C_n = A_n + i B_n $
\[
	\omega (z) = (V_x - i V_y) ( r \cos\theta + ir \sin\theta ) + (A + iB)(\ln r + i\theta) +
		\sum_{ 1 }^{ \infty } \frac{ A_n + iB_n }{ r^n } (\cos n\theta - i\sin n\theta)
\]
\par Выделим отсюда вещественную часть:
\[
	\varphi (r,\theta) = V_x r \cos\theta + V_y r \sin\theta + A\ln r - B\theta +
		\sum_{ 1 }^{ \infty } \frac{ A_n \cos n\theta + B_n \sin n\theta }{ r^n }
\]
\par Продифференцируем полученное выражение по $r$ и сделаем подстановку $r=R$, чтобы получить граничное условие.
\[
	\left. \partd{ \varphi }{ r } \right|_{ r=R } =
		V_x\cos\theta + V_y \sin\theta + \frac{ A }{ r } -
			\sum_{ 1 }^{ \infty } \frac{ nA_n \cos n \theta  + nB_n \sin n\theta }{ R^{ n+1 } }
	= U_x \cos\theta + U_y \sin\theta
\]
\par Получили два тригонометрических ряда. Они совпадают тогда и только тогда, когда совпадают все коэффициенты.
\[
	\left\{\begin{aligned}
		V_x - \frac{ A_1 }{ R^2 } &= U_x \\
		V_y - \frac{ B_1 }{ R^2 } &= U_y \\
	\end{aligned}\right.
	\Rightarrow
	\left\{\begin{aligned}
		A_1 &= R^2 \left( V_x - U_x \right)\\
		B_1 &= R^2 \left( V_y - U_y \right)
	\end{aligned}
	\right.
\]
\par Воспользовавшись полученными знаниями запишем комплексный потенциал:
\[
	\omega ( z ) = ( V_x - i V_y ) z + iB \ln z + \frac{ (V_x - U_x)R^2 + i (V_y - U_y)R^2 }{ z }
\]
\par Это выражение станет гораздо более наглядным, если ввести комплексные переменные, соответствующие векторам:
\[
	\begin{aligned}
		V &= V_x + i V_y\\
		U &= U_x + i U_y\\
	\end{aligned}
\]
\par Тогда комплексный потенциал примет вид:
\begin{equation}
\myboxed{
	\omega(z) = \conj{V} z + \left( V - U \right) \frac{ R^2 }{ z } + \frac{ \Gamma }{ 2 \pi i} \ln z
}
\end{equation}
\par Рассмотрим частные случаи комплексного потенциала:

\subsubsection{Неподвижный цилиндр в покоящейся жидкости $U=0;V=0$.}
\begin{equation}
	\omega(z) = \frac{ \Gamma }{ 2 \pi i } \ln z
\end{equation}

\par Потенциал совпадает с точечным вихрем.
\begin{center}
	\begin{tikzpicture}[scale = .8]
		\draw [pattern = north west lines, pattern color = black!30] (0,0) circle (.8);
		\draw [->] (-2,0) -- (2,0) node [below] {$x$};
		\draw [->] (0,-2) -- (0,2) node [left]  {$y$};
		\foreach \x in {.8, 1.1,..., 1.7}
			\draw [marrow, blue, thin] (\x,0) arc (0:360:\x);
	\end{tikzpicture}	
\end{center}


\subsubsection{Движущийся цилиндр в покоящейся жидкости $V=0; U=U_x>0$.}
\begin{equation}
	\omega(z) = - U_x \frac{ R^2 }{ z } + \frac{ \Gamma }{ 2 \pi i }\ln z
\end{equation}
\par Пусть $\Gamma = 0$. Тогда потенциал совпадает с дипольным.
\begin{equation}
	\omega(z) = - U_x \frac{ R^2 }{ z }
\end{equation}
\begin{center}
	\begin{tikzpicture}[scale = .8]
		\begin{scope}
			\clip (-3,-3) rectangle (3,3);
			\foreach \x in {0,.2,...,5}{
				\draw [marrow, blue] (0,0) arc (-90:270:\x);
				\draw [marrow, blue] (0,0) arc (90:-270:\x);	
			}
		\end{scope}
		\draw [fill=white] (0,0) circle (.8);
		\draw [pattern = north west lines, pattern color = black!30] (0,0) circle (.8);
		\draw [->] (-3,0) -- (3,0) node [below] {$x$};
		\draw [->] (0,-3) -- (0,3) node [left]  {$y$};	
	\end{tikzpicture}
\end{center}



\subsubsection{Неподвижный цилиндр в потоке $U=0;V=V_x>0$.}
\begin{equation}
	\omega (z) = V\left( z + \frac{ R^2 }{ z } \right) + \frac{ \Gamma }{ 2 \pi i } \ln z
\end{equation}
\par Рассмотрим сперва случай \textbf{безциркуляционного} обтекания.
\begin{equation}
	\omega (z) = V \left( z + \frac{ R^2 }{ z } \right)
\end{equation}

\[
	\omega (z) = \varphi + i \psi = V_x \left( x + iy + \frac{ R^2 ( x - i y ) }{ x^2 + y^2} \right)
\]

\[
	\begin{aligned}
		\varphi &= V_x \left( x + \frac{ R^2 x }{ x^2 + y^2} \right)\\
		\psi    &= V_x \left( y - \frac{ R^2 y }{ x^2 + y^2} \right)
	\end{aligned}
\]
\par Видно, что линии тока  $\psi=const$ симметричны относительно осей (замена $x\rightarrow-x;y\rightarrow-y$ меняет только константу). Так же можно заметить, что линия $\psi = 0$ соответствует поверхности цилиндра.
\par Чтобы найти скорости перейдем в полярную систему координат:
\[
	\varphi = V_x \left( r + \frac{ R^2 }{ r } \right)\cos\theta
	\Rightarrow
	\left\{\begin{aligned}
		v_r 	 &= \partd{ \varphi }{ r } = V_x \left( 1 - \frac{ R^2 }{ r^2 } \right) \cos\theta\\
		v_\theta &= \frac{ 1 }{ r } \partd{ \varphi }{ \theta }=
					- V_x \left( 1 + \frac{ R^2 }{ r^2 } \right) \sin\theta
	\end{aligned}\right.
\]
\par При $r = R$
\begin{equation}
	\left\{\begin{aligned}
		v_n &= 0\\
		v_\theta &= - 2 V \sin\theta
	\end{aligned}\right.
\end{equation}

Обратим внимание, что есть две точки, в которых скорость равна $2V$. Этим точкам соответствует $\theta = \pm \pi/2$.

\begin{center}
		\begin{tikzpicture}[scale = .8]
		\draw [pattern = north west lines, pattern color = black!30] (0,0) circle (.8);
		\draw [->] (-3,0) -- (3,0) node [below] {$x$};
		\draw [->] (0,-3) -- (0,3) node [left]  {$y$};
		\foreach \C in {.5, 1, ..., 2.5}{
			\draw [blue, marrow](-3, \C) to (-1.2,\C) to [out = 0, in = 180]
						(0, { \C + 0.15*(3-\C) })  to [out = 0, in = 180] (1.2,\C) to (3,\C);
			\draw [blue, marrow](-3, -\C) to (-1.2,-\C) to [out = 0, in = 180]
						(0, { -\C - 0.15*(3-\C) }) to [out = 0, in = 180] (1.2,-\C) to (3,-\C);
		}
		\draw [blue, marrow] (-3,0) -- (-.8,0) arc (180:0:.8) -- (3,0);
		\draw [blue, marrow] (-3,0) -- (-.8,0) arc (-180:0:.8) -- (3,0);
		\end{tikzpicture}
\end{center} 